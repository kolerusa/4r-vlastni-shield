# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 11 hodin |
| jak se mi to podařilo rozplánovat | dobře, začal jsem včas |
| design zapojení | https://gitlab.spseplzen.cz/kolerusa/4r-vlastni-shield/-/blob/main/dokumentace/schema/Fritzing_zapojeni.png |
| proč jsem zvolil tento design | přišel mi jako nejjednodušší k realizaci |
| zapojení | https://gitlab.spseplzen.cz/kolerusa/4r-vlastni-shield/-/blob/main/dokumentace/design/IMG_3291.jpg |
| z jakých součástí se zapojení skládá | LCD display, RGB pásek, resistor, teploměr, Wemos D1R2, 2x LED |
| realizace | https://gitlab.spseplzen.cz/kolerusa/4r-vlastni-shield/-/blob/main/dokumentace/fotky/IMG_3287.jpg|
| nápad, v jakém produktu vše propojit dohromady| teploměr |
| co se mi povedlo | všechno |
| co se mi nepovedlo/příště bych udělal/a jinak | vše se povedlo |
| zhodnocení celé tvorby | jsem se svou prací spokojený |
