#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2); // nastav adresu LDC na 0x27 pro 16 sloupcovy a 2 radkovy displej

const uint32_t pauza = 5000;

uint16_t zbytek;
const uint16_t delitel = 1000;

unsigned long casovaZnacka;

void obrazovka1()
{
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("IOT PROJEKT"); // napis na displej "NAVODY  DRATEK"
  lcd.setCursor(3, 1);
  lcd.print("HAUT JE KOKOT");      // napis na displej "DRATEK.CZ"
}

void obrazovka2()
{
  zbytek = millis() % delitel;
  if (zbytek < (pauza/delitel))
  {
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("CAS OD ZAPNUTI"); // napis na displej "CAS OD ZAPNUTI"
    lcd.setCursor(1, 1);
    lcd.print(millis() / 1000);
    lcd.setCursor(12, 1);
    lcd.print("sec");            // napis na displej "sec"
  }
}

void setup()
{
  lcd.init();           // initialize the lcd
  lcd.backlight();      // zapni podsvĂ­cenĂ­
  lcd.setCursor(4, 0);  // dej kurzor na sloupec 1, Ĺ™Ăˇdek 0
  lcd.print("ARDUINO"); // napis na displej "ARDUINO"
  delay(pauza);
  casovaZnacka = millis(); // vytvori casovou znacku
}

void loop()
{
  if (millis() < casovaZnacka + pauza) // jestli je aktualni cas < nez casova znacka + pauza (5000 ms);
  {
    obrazovka2();
  }
  else
  {
    obrazovka1();
    delay(pauza);            // pauza 5000 ms
    casovaZnacka = millis(); // vytvori casovou znacku
  }
}