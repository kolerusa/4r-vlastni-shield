#include <OneWire.h>            //načtení knihovny
#include <DallasTemperature.h>
#include <Adafruit_NeoPixel.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2); // nastav adresu LCD na 0x27 pro 16 sloupcovy a 2 radkovy displej

const int pinCidlaDS = D3;

const int pResistor = A0;

const int ledBlue = D1;
const int ledGreen = D5;


#define pinDIN D6
#define pocetLED 8

Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(pocetLED, pinDIN, NEO_GRB + NEO_KHZ800);

int value;			//celý číslo
float teplota;	//reálné číslo

OneWire oneWireDS(pinCidlaDS);
DallasTemperature senzoryDS(&oneWireDS);

void hodnoty()      //metoda co vypisuje na LCD
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Photores.:");  // slovícko
  lcd.print(value); // hodnota
  lcd.setCursor(0, 1);
  lcd.print("Teplota:");
  lcd.print(teplota);     
  lcd.print("'C");
}


void setup() {
  Serial.begin(115200);
  senzoryDS.begin();
  pinMode(pResistor, INPUT);
  pinMode(ledBlue, OUTPUT);
  pinMode(ledGreen, OUTPUT);

   

  rgbWS.begin();
  nastavRGB(255, 255  , 255  , 1);
  delay(500);
  nastavRGB(255  , 255, 255  , 2);
  delay(500);
  nastavRGB(255  , 255  , 255, 3);
  delay(500);
  nastavRGB(0  , 255  , 0  , 1);
  nastavRGB(255  , 0  , 0  , 2);
  nastavRGB(0  , 0  , 255  , 3);
  delay(500);

  lcd.init();           // inicializace lcd
  lcd.backlight();      // zapni podsvi­ceni­
  lcd.setCursor(4, 0);  // dej kurzor na sloupec 1, radek 0
  lcd.print("ARDUINO"); // napis na displej "ARDUINO"
}

void loop() {
  value = analogRead(pResistor);
  Serial.print("photo ");
  Serial.println(value);
  senzoryDS.requestTemperatures();
  
  Serial.print("Teplota cidla: ");
  Serial.print(senzoryDS.getTempCByIndex(0));
  Serial.println("stupnu Celsia");
  teplota = senzoryDS.getTempCByIndex(0);

  hodnoty();

  digitalWrite(ledBlue, HIGH);    //zapíná LED
  digitalWrite(ledGreen, HIGH);

  delay(500);


  byte cervena = random(0, 256);        //nastavuje random hodnotu od 0 - 256
  byte zelena  = random(0, 256);
  byte modra   = random(0, 256);
  byte dioda   = random(1, (pocetLED + 1));
  nastavRGB(cervena, zelena, modra, dioda);
}

void nastavRGB (byte r, byte g, byte b, int cislo) {    //metoda nastavuje RGB barvy
  uint32_t barva;
  barva = rgbWS.Color(r, g, b);
  rgbWS.setPixelColor(cislo - 1, barva);
  rgbWS.show();
}

