// připojení potřebné knihovny
#include <Adafruit_NeoPixel.h>
// nastavení propojovacího pinu
#define pinDIN D6
// nastavení počtu LED modulů
#define pocetLED 8
// inicializace LED modulu z knihovny
Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(pocetLED, pinDIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // zahájení komunikace s LED modulem
  rgbWS.begin();
  // nastavení barvy na první LED diodu
  delay(100);
  nastavRGB(255, 0  , 0  , 1);
  delay(1000);
  nastavRGB(255, 255  , 0  , 2);
  delay(1000);
  nastavRGB(255, 255  , 255  , 3);
  delay(1000);
  nastavRGB(0, 255  , 0  , 4);
  delay(1000);
  nastavRGB(145, 20  , 90  , 5);
  delay(1000);
  nastavRGB(255, 0  , 255  , 6);
  delay(1000);
  nastavRGB(0, 0  , 255  , 7);
  delay(1000);
  nastavRGB(255, 0  , 255  , 8);
  delay(2000);

  // nastavení černé barvy na první 3 LED diody = vypnutí
  nastavRGB(255  , 0  , 0  , 1);
  nastavRGB(255  , 255  , 0  , 2);
  nastavRGB(0  , 255  , 255  , 3);
  nastavRGB(255  , 0  , 255  , 4);
  nastavRGB(0  , 154  , 0  , 5);
  nastavRGB(25  , 25  , 25  , 6);
  nastavRGB(0  , 255  , 0  , 7);
  nastavRGB(255  , 255  , 255  , 8);
  delay(500);
}
void loop() {

}
// funkce pro nastavení zadané barvy na zvolenou diodu
void nastavRGB (byte r, byte g, byte b, int cislo) {
  // vytvoření proměnné pro ukládání barev
  uint32_t barva;
  // načtení barvy do proměnné
  barva = rgbWS.Color(r, g, b);
  // nastavení barvy pro danou LED diodu,
  // číslo má pořadí od nuly
  rgbWS.setPixelColor(cislo - 1, barva);
  // aktualizace barev na všech modulech
  rgbWS.show();
}